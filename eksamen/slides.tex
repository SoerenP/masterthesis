\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{graphics}


%%%FORMAL STUFF
\title[Crisis] % (optional, only for long titles)
{Using ORAM in MPC}
\subtitle{Enabling Efficient Oblivious Access in Secure Computation}
\subject{Computer Science}
\author[Author, Anders] % (optional, for multiple authors)
{Søren Elmely Pettersson}

%%%%SLIDES
\begin{document}
\frame{\titlepage}

%%%%TODO: add text animation 

\begin{frame}[shrink]

\frametitle{ORAM}
\framesubtitle{Motivation and Intuition}
\begin{columns}[T]
\begin{column}[T]{5cm}
\begin{block}{Software Protection}
\begin{itemize}[<+->]
\item Assume Shielded CPU
\item Need to access untrusted external memory
\item Use encryption!
\item Data request sequence $\vec{y}$ 
\item Access pattern $A(\vec{y})$ can leak information
\end{itemize}
\end{block}
\pause
How to prevent information leakage? 
\end{column}
\begin{column}[T]{5cm}
\includegraphics[scale=0.4]{binsearchaccess.png}
\pause
\begin{block}{Oblivious RAM}
\begin{itemize}[<+->]
\item Hide $\vec{y}$ from observer.
\item Require $A(\vec{y}) \approx A(\vec{x})$ iff $|\vec{y}| = |\vec{x}|$ and correctness
\item Implies Oblivious Simulation Of RAM Programs
\item Software Protection
\item CPU and Memory $\Rightarrow$ Client And Server
\end{itemize}
\end{block}
\end{column}
\end{columns}
\end{frame}


\begin{frame}[shrink]

\frametitle{ORAM}
\framesubtitle{Solutions}
\begin{columns}[T]

\begin{column}[T]{5cm}
\begin{block}{Trivial Solution}
\begin{itemize}[<+->]
\item Read And Write Entire Memory on each Access
\item Semantically Secure Encryption
\item Writing new data $\approx$ re-encryption of old data
\item $A(\vec{y}) \approx A(\vec{x})$ if $|\vec{y}| = |\vec{x}|$
\item $O(N)$ Overhead
\end{itemize}
\end{block}
\end{column}

\begin{column}[T]{5cm}
\includegraphics[scale=0.4]{trivial.png}
\pause
\begin{block}{Overhead}
We want \emph{efficient} oblivious access $\Rightarrow$ low bandwidth overhead.
\end{block}
\pause
\begin{block}{How low can you go?}
Lower bound of $O(\log N)$ given certain assumptions/restrictions.    
\end{block}
\end{column}
\end{columns}    
\end{frame}


\begin{frame}[shrink]{Hierarchical ORAM}
\begin{columns}[T]
\begin{column}[T]{5cm}
\begin{itemize}[<+->]
\item Server stores levels of increasing size
\item Client evaluates PRF to access hierarchy (hash tables)
\item As levels fill up, we reshuffle into lower, larger levels
\item Security argument: If the requests to a level uses unique $v$'s in between reshuffling, access given PRF $\approx$ uniformly random access.
\end{itemize}
\end{column}
\begin{column}[T]{5cm}
\includegraphics[scale=0.40]{treeoram-1.png}
\newline
\pause
Ensure by design that each level is accessed in such a way between reshuffling.
\pause
\begin{block}{Distributed ORAM}
$O(\log N)$ bandwidth overhead for $O(N)$ server storage with $O(1)$ client storage, using multiple computing servers.  
\end{block}
\end{column}
\end{columns}
\end{frame}



\begin{frame}[shrink]{Tree-Based ORAM}
\begin{columns}[T]
\begin{column}[T]{5cm}
\begin{block}{Server Side Data Structure}
\begin{itemize}[<+->]
\item Binary Tree with $2^{L}$ leaves
\item Nodes are buckets storing $B$ data blocks $(v,d,z)$.
\item Pad buckets to full capacity.
\end{itemize}
\end{block}
\pause
\begin{block}{Invariant}
The block $(v,d,z)$ lies on the the unique path to leaf $z \in \{0,...,2^{L}-1\}$.
\end{block}
\pause
\begin{block}{Client Storage}
\begin{itemize}[<+->]
\item Position map $pos[v] = z$ enables access.
\item Stash to store overflow. 
\end{itemize}
\end{block}
\pause
\end{column}
\begin{column}[T]{5cm}
\includegraphics[scale=0.40]{treebased.png}
\begin{block}{Access}
$Access(v) = $ Read $Path(pos[v])$, find $(v,d,z)$. Remap $pos[v] = z'$ uniformly at random.
\end{block}
\pause
\begin{block}{Eviction}
Redistribute Entries throughout tree to prevent overflow of stash. 
\end{block}
\end{column}
\end{columns}
\end{frame}


\begin{frame}[shrink]{Tree-Based ORAM}
\begin{block}{Access Pattern}
$A(\vec{y}) \approx$ uniformly random paths in server side tree. 
\end{block}
\pause
\begin{block}{Recursion}
To avoid storing $O(N)$ position map, store $x > 1$ leaf mappings per memory index in new ORAM. Implies $O(\log_x N)$ levels of recursion. 
\end{block}
\pause
\begin{block}{Path ORAM: Bound on Overflow}
\begin{itemize}
\item Greedy Eviction on Read Path
\item Stash size $O(\log N)\omega(1)$, $B=4 \Rightarrow negl(N)$ probability of overflow.
\end{itemize}
\end{block}		
\pause
\begin{block}{Path ORAM: Bandwidth Overhead}
$O(\log^2 N)$ for certain parameter ranges. 
\end{block}	
\end{frame}

\begin{frame}[shrink]
\frametitle{MPC}
\framesubtitle{Intuition}
\begin{block}{Multi Party Computation}
\begin{itemize}[<+->]
\item Players $P_1,...,P_M$ wish to compute the function $f(x^1,...,x^M) = (y^1,...,y^M)$.
\item Goal: leak no information during secure computation.
\end{itemize} 
\end{block}
\pause
\begin{block}{Secret Sharing}
\begin{itemize}[<+->]
\item Split secret $[x]$ into shares $x_1,...,x_M$.
\item (Linear) combination of $t+1$ shares $\Rightarrow$ reconstruct $[x]$.
\item $t$ or fewer shared $\Rightarrow$ no information about $[x]$. 
\item We can add and multiply secrets by manipulating our shares!
\end{itemize}
\end{block}
\pause
\begin{block}{Circuit Model}
\begin{itemize}[<+->]
\item Addition and multiplication secure.
\item Formulate function as a binary or arithmetic circuit.
\item Circuit Complexity $!=$ RAM complexity
\item Multiplication implies messages between players (I/O).
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[shrink]
\frametitle{MPC}
\begin{block}{Oblivious Access to Memory in MPC}
\begin{itemize}[<+->]
\item Secret are hidden/shared
\item Computation on elements \emph{not} hidden
\item Accessing memory directly leaks information
\end{itemize}
\end{block}

\pause
\begin{block}{Trivial Solution}
\begin{itemize}[<+->]
\item Make computation of memory access depend on all secret of memory 
\item E.g: Inner product with secret index vector. 
\includegraphics[scale=0.4]{innerprod.png}

\item $O(N)$ overhead
\item RAM program secure computation gets $O(N)$ blowup
\end{itemize}
\end{block}
\pause
We want \emph{efficient} oblivious access. Sounds familiar?
\end{frame}


\begin{frame}[shrink]
\frametitle{Using ORAM in MPC}
Idea: Secure Computation of ORAM!
\newline
\pause
\begin{block}{Sharing the Client - Ostrovsky/Shoup Compiler}
\begin{itemize}[<+->]
\item Securely compute client of ORAM
\item Keep server(s) locally at players
\item Secure to reveal Access protocol to servers
\item We need to do encryption/decryption \emph{inside} MPC
\end{itemize}
\end{block}
\pause
\begin{block}{Sharing Everything}
\begin{itemize}[<+->]
\item Both Client and Server inside MPC
\item Get Encryption for free
\item Easier active security
\item Cannot leverage server side computation to same extend
\end{itemize}
\end{block}
\pause
Lets take the best ORAM from RAM model and us it in MPC!
\end{frame}

\begin{frame}[shrink]{Example: Distributed ORAM}
\begin{block}{$O(\log N)$ amortized bandwidth and only $O(1)$ client storage!}
Server side computation fits Ostrovsky-Shoup
\end{block}
\pause
\begin{block}{Shared Client state $\Rightarrow$ Secure computation of PRF on shared input}
\begin{itemize}[<+->]
\item State of the art (soPRF) requires $O(t)$ OTs for $O(t)$ sized input
\item Must evaluate for access to each of $O(\log N)$ levels of server hierarchy.
\item Large constants hidden in amortized $O(\log N)$ bandwidth.
\item worst case bandwidth cost can be $O(N)$ for large levels. 
\item Seems hard to leverage the required encryption of Ostrovsky-Shoup due to the nature of Distributed ORAM.
\end{itemize}
\end{block}
\end{frame}


\begin{frame}[shrink]
\frametitle{Example: Path ORAM}
Idea: Tree ORAMs do not need PRF!
\pause
\begin{block}{Path ORAM is "the best" Tree ORAM}
\begin{itemize}[<+->]
\item Sample Leaf Mappings in MPC
\item Recursive Solution to access shared position map. 
\item Eviction logic can be implemented with oblivious sorting.
\end{itemize}
\end{block}
\pause
\begin{block}{Is low bandwidth the goal? (SCORAM)}
\begin{itemize}[<+->]
\item Path ORAM $O(\log^2 N)$ bandwidth
\item Binary Tree ORAM $O(\log^3 N)$ bandwidth
\item Binary Tree ORAM circuit $<$ Path ORAM Circuit 
\item Path ORAM has a complex eviction circuit
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[shrink]
\frametitle{Using ORAM in MPC}
\begin{block}{Problem: Bandwidth overhead no longer most important metric.}
\begin{itemize}[<+->]
\item Evaluate entire circuit for ORAM/client with MPC
\item Circuit will be bandwidth but also computation!
\item Usual Computation/bandwidth trade-off becomes moot.
\item Multiplication implies I/O.
\end{itemize}
\end{block}
\pause
\begin{block}{Circuit Size Metric}
\begin{itemize}[<+->]
\item Total circuit size of the ORAM client's next algorithm over all execution rounds during each ORAM request.
\item Original Lower Bound applies to new metric (SCORAM).
\item $O(F(N))$ circuit complexity $\Rightarrow$ $O(F(N))$ bandwidth overhead.
\item $O(F(N))$ bandwidth overhead $!=$ $O(F(N))$ circuit complexity.
\end{itemize}
\end{block}
\end{frame}

%Try hierarchical, it has lowest overhead
%then try tree oram?

\begin{frame}[shrink]
\frametitle{Avoid Complex Functionalities}
\framesubtitle{Shared Client State}
\begin{block}{Want low bandwidth and low computation complexity}
But RAM model ORAMs abuse computation to get low bandwidth..
\end{block}
\pause
\begin{block}{Important Difference between MPC and RAM}
\begin{itemize}
\item RAM model shielded-CPU vs. MPC shared client.
\item Bandwidth not end goal, circuit size is.
\end{itemize}
\end{block}
\begin{block}{New Tradeoff?}
Low bandwidth + Complex Computation vs. Higher bandwidth + Simpler 
\end{block}
\begin{block}{Or can we get both?}
Low bandwidth + Low Computation?
\end{block}
\end{frame}


\begin{frame}
\frametitle{Altering Tree ORAM to Circuit Metric}
Make Tree-Based ORAM fit even better to MPC
\begin{block}{We want simple eviction}
But still effective!
\end{block}
\pause
\begin{block}{SCORAM and Circuit ORAM - Simple and Effective Eviction}
\begin{itemize}[<+->]
\item Alter eviction to single pass strategy (avoid oblv. sort)
\item Read meta-data only $\Rightarrow$ compute eviction movement 
\item Between $5$ and $50$ times smaller circuit compared to other Tree-Based ORAMs when implemented with Yao's Garbled Circuits.
\item Circuit ORAM achieves 2 sec per ORAM operation for $N=2^{20}$.
\item Keller \& Scholl Path ORAM $250 ms$ with $100-800$ min (SPDZ) preprocessing.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}[shrink]{Altering Tree ORAM}
\begin{block}{3PORAM - Honest Majority}
\begin{itemize}[<+->]
\item Starting point is Tree-Based ORAM.
\item 3 players and honest majority assumption.
\item Novel (easy) OT techniques due to honest majority.
\item Eviction logic only requires 17 non-xor gates
\item Eviction movement $\approx$ 4 times transfer of a path. 
\item Access protocol outside secure computation $\approx$ symmetric encryption and transfer of a Path.
\item Small circuit for $N=2^{20}$, $96k$ vs. $0.9M$ (Circuit ORAM).
\item $320 ms$ per ORAM operation for $N = 2^{36}$ with Yao's Garbled Circuits and $1.2$ sec of preprocessing (Garbling).  
\end{itemize}
\end{block}
\pause
Heavily abuses the honest majority. What about dishonest majority and $N$ players? 
\end{frame}
\end{document}