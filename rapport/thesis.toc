\select@language {american}
\contentsline {chapter}{Abstract}{iii}
\contentsline {chapter}{Acknowledgments}{v}
\contentsline {chapter}{Notation and Assumptions on the Reader}{1}
\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {1.1}A Map to the Thesis}{5}
\contentsline {chapter}{\numberline {2}Background \& Related Work}{7}
\contentsline {chapter}{\numberline {3}Oblivious RAM}{11}
\contentsline {section}{\numberline {3.1}Intuition}{11}
\contentsline {section}{\numberline {3.2}Definition}{13}
\contentsline {subsection}{\numberline {3.2.1}RAM, CPU and Memory}{13}
\contentsline {subsection}{\numberline {3.2.2}Access Pattern}{15}
\contentsline {subsection}{\numberline {3.2.3}Oblivious RAM}{16}
\contentsline {subsection}{\numberline {3.2.4}Simulation}{16}
\contentsline {subsection}{\numberline {3.2.5}Negligible}{18}
\contentsline {subsection}{\numberline {3.2.6}Security}{18}
\contentsline {subsection}{\numberline {3.2.7}Adversarial Model}{19}
\contentsline {subsection}{\numberline {3.2.8}Security Through Experiment}{19}
\contentsline {subsection}{\numberline {3.2.9}Lower Bound}{20}
\contentsline {subsection}{\numberline {3.2.10}Simulation Overhead}{21}
\contentsline {section}{\numberline {3.3}Metrics for ORAM}{21}
\contentsline {subsection}{\numberline {3.3.1}Bandwidth Overhead}{22}
\contentsline {subsection}{\numberline {3.3.2}Block size}{22}
\contentsline {subsection}{\numberline {3.3.3}Access Complexity}{22}
\contentsline {subsection}{\numberline {3.3.4}Client Storage}{23}
\contentsline {subsection}{\numberline {3.3.5}Client and Server Computation}{24}
\contentsline {section}{\numberline {3.4}A Tree-Based ORAM Scheme - Path ORAM}{24}
\contentsline {subsection}{\numberline {3.4.1}Intuition}{25}
\contentsline {subsection}{\numberline {3.4.2}Server}{25}
\contentsline {subsection}{\numberline {3.4.3}Client}{26}
\contentsline {subsection}{\numberline {3.4.4}Access}{26}
\contentsline {subsection}{\numberline {3.4.5}Eviction}{27}
\contentsline {subsection}{\numberline {3.4.6}Security}{28}
\contentsline {subsection}{\numberline {3.4.7}Recursion}{29}
\contentsline {subsection}{\numberline {3.4.8}Bandwidth Overhead}{30}
\contentsline {subsection}{\numberline {3.4.9}Client And Server Computation}{31}
\contentsline {subsection}{\numberline {3.4.10}Recap}{31}
\contentsline {section}{\numberline {3.5}A Hierarchical ORAM Scheme - Distributed ORAM}{32}
\contentsline {subsection}{\numberline {3.5.1}Intuition}{32}
\contentsline {subsection}{\numberline {3.5.2}Tagging \& Hashing}{33}
\contentsline {subsection}{\numberline {3.5.3}Servers}{34}
\contentsline {subsection}{\numberline {3.5.4}Client}{36}
\contentsline {subsection}{\numberline {3.5.5}Access}{36}
\contentsline {subsection}{\numberline {3.5.6}Reshuffling}{38}
\contentsline {subsection}{\numberline {3.5.7}Security}{39}
\contentsline {subsubsection}{Reshuffling}{40}
\contentsline {subsubsection}{Access}{40}
\contentsline {subsubsection}{Conclusion}{41}
\contentsline {subsection}{\numberline {3.5.8}Access Overhead}{42}
\contentsline {subsection}{\numberline {3.5.9}Client and Server Computation}{42}
\contentsline {subsection}{\numberline {3.5.10}Comparison to Path ORAM}{43}
\contentsline {section}{\numberline {3.6}Tamper Resistance}{44}
\contentsline {section}{\numberline {3.7}Lower Bound Revisited}{46}
\contentsline {subsection}{\numberline {3.7.1}Assumptions and Restrictions}{46}
\contentsline {subsection}{\numberline {3.7.2}Altering the model}{47}
\contentsline {subsubsection}{Multiple servers}{48}
\contentsline {subsubsection}{Server Computation}{50}
\contentsline {chapter}{\numberline {4}Multi Party Computation}{53}
\contentsline {section}{\numberline {4.1}Intuition}{53}
\contentsline {section}{\numberline {4.2}Computational Model}{54}
\contentsline {section}{\numberline {4.3}Secret Sharing}{55}
\contentsline {subsection}{\numberline {4.3.1}Example: Shamir Secret Sharing}{56}
\contentsline {section}{\numberline {4.4}Security \& Universal Composability}{58}
\contentsline {subsection}{\numberline {4.4.1}Simulation}{58}
\contentsline {subsection}{\numberline {4.4.2}Entities: Players, Adversaries and Environments}{59}
\contentsline {subsection}{\numberline {4.4.3}Real World}{63}
\contentsline {subsection}{\numberline {4.4.4}Ideal World}{63}
\contentsline {subsection}{\numberline {4.4.5}Hybrid World}{64}
\contentsline {subsection}{\numberline {4.4.6}Security}{64}
\contentsline {subsection}{\numberline {4.4.7}Feasibility Results}{67}
\contentsline {section}{\numberline {4.5}Arithmetic Black Box}{68}
\contentsline {subsection}{\numberline {4.5.1}$F_{ABB}$}{69}
\contentsline {subsubsection}{Notation}{71}
\contentsline {subsection}{\numberline {4.5.2}$F_{ABB + RAM}$}{72}
\contentsline {subsection}{\numberline {4.5.3}Environment Class $Env_{\text {Init $P_1$}}$}{75}
\contentsline {section}{\numberline {4.6}Metrics for MPC}{76}
\contentsline {subsection}{\numberline {4.6.1}Computation and Circuit Complexity}{76}
\contentsline {subsection}{\numberline {4.6.2}Round Complexity}{77}
\contentsline {subsection}{\numberline {4.6.3}Communication Complexity}{77}
\contentsline {subsection}{\numberline {4.6.4}Storage Complexity}{77}
\contentsline {subsection}{\numberline {4.6.5}Black Box Complexity}{77}
\contentsline {section}{\numberline {4.7}Regarding Secure Two-Party Computation}{78}
\contentsline {section}{\numberline {4.8}Example Schemes}{79}
\contentsline {subsection}{\numberline {4.8.1}BeDOZA}{79}
\contentsline {subsection}{\numberline {4.8.2}Garbled Circuits}{79}
\contentsline {subsection}{\numberline {4.8.3}MPC on Binary Circuits}{80}
\contentsline {chapter}{\numberline {5}Primitives for ORAM in MPC}{83}
\contentsline {section}{\numberline {5.1}Intuition and Motivation}{83}
\contentsline {section}{\numberline {5.2}Enabling Efficient MPC of RAM programs}{84}
\contentsline {subsection}{\numberline {5.2.1}The Ostrovsky-Shoup Compiler}{84}
\contentsline {subsubsection}{The Compiler}{86}
\contentsline {subsection}{\numberline {5.2.2}The Client-Server Setting}{87}
\contentsline {subsection}{\numberline {5.2.3}Secret Sharing Everything}{91}
\contentsline {section}{\numberline {5.3}$F_{ORAM}$}{92}
\contentsline {section}{\numberline {5.4}Metrics for ORAM in MPC}{94}
\contentsline {section}{\numberline {5.5}Lower Bound Revisited, Again}{99}
\contentsline {section}{\numberline {5.6}Recap}{101}
\contentsline {chapter}{\numberline {6}Secret Sharing the Entire ORAM}{103}
\contentsline {section}{\numberline {6.1}Security Model}{104}
\contentsline {section}{\numberline {6.2}Trivial Solution}{105}
\contentsline {subsubsection}{Security}{106}
\contentsline {section}{\numberline {6.3}Example: Secret Sharing Path ORAM}{107}
\contentsline {subsection}{\numberline {6.3.1}Position Map}{107}
\contentsline {subsection}{\numberline {6.3.2}Indexing Into Position Map Memory Cells}{108}
\contentsline {subsection}{\numberline {6.3.3}Multiple Field Elements per Memory Cell}{109}
\contentsline {subsection}{\numberline {6.3.4}Multiple Leaf Mappings per Field Element}{109}
\contentsline {subsection}{\numberline {6.3.5}Overall Position Map Access}{110}
\contentsline {subsection}{\numberline {6.3.6}Eviction}{111}
\contentsline {subsection}{\numberline {6.3.7}The Path ORAM Construction}{112}
\contentsline {subsubsection}{Storage}{112}
\contentsline {subsubsection}{Access}{112}
\contentsline {subsection}{\numberline {6.3.8}Security}{114}
\contentsline {subsubsection}{Access patterns}{114}
\contentsline {subsubsection}{Correctness}{115}
\contentsline {subsubsection}{Simulation}{115}
\contentsline {subsection}{\numberline {6.3.9}Client Side Circuit and Computation}{117}
\contentsline {subsection}{\numberline {6.3.10}Empirical Results}{118}
\contentsline {section}{\numberline {6.4}Recap}{119}
\contentsline {chapter}{\numberline {7}Secret Sharing the ORAM Client}{121}
\contentsline {section}{\numberline {7.1}Example: Distributed ORAM}{121}
\contentsline {subsection}{\numberline {7.1.1}The Distributed ORAM Construction}{122}
\contentsline {subsubsection}{The Client's Access Protocol}{123}
\contentsline {subsubsection}{Reshuffle}{125}
\contentsline {subsection}{\numberline {7.1.2}Security}{125}
\contentsline {subsubsection}{Passive Adversaries}{126}
\contentsline {subsubsection}{Access}{127}
\contentsline {subsubsection}{Reshuffle}{127}
\contentsline {subsubsection}{Malicious Adversaries}{129}
\contentsline {subsection}{\numberline {7.1.3}Practical Efficiency}{129}
\contentsline {section}{\numberline {7.2}Secure Computation of PRFs}{131}
\contentsline {subsection}{\numberline {7.2.1}Practical Efficiency}{132}
\contentsline {subsection}{\numberline {7.2.2}Shared Oblivious PRF}{133}
\contentsline {subsubsection}{soPRF}{134}
\contentsline {subsubsection}{Complexity}{135}
\contentsline {section}{\numberline {7.3}Avoiding PRFs}{136}
\contentsline {subsection}{\numberline {7.3.1}Replacing the Tagging PRF in Distributed ORAM}{137}
\contentsline {section}{\numberline {7.4}Leveraging Encryption \& Decryption Inside Secure Computation}{139}
\contentsline {subsection}{\numberline {7.4.1}Avoiding Encryption in the Secure Computation of Distributed ORAM}{140}
\contentsline {section}{\numberline {7.5}Recap}{141}
\contentsline {chapter}{\numberline {8}Tailoring ORAM to MPC}{145}
\contentsline {section}{\numberline {8.1}Altering Tree-Based ORAM to MPC - SCORAM and Circuit ORAM}{147}
\contentsline {subsection}{\numberline {8.1.1}SCORAM - Avoiding Complex Eviction}{148}
\contentsline {subsubsection}{SCORAM Eviction}{148}
\contentsline {subsubsection}{Parameter Optimization}{149}
\contentsline {subsection}{\numberline {8.1.2}Circuit ORAM - Obtaining Bounds}{150}
\contentsline {subsubsection}{Single Pass Eviction }{150}
\contentsline {subsubsection}{Parameterization and Bounds}{151}
\contentsline {subsubsection}{Practical Efficiency}{151}
\contentsline {section}{\numberline {8.2}Assuming an Honest Majority - 3PORAM}{152}
\contentsline {subsection}{\numberline {8.2.1}Tree-Based ORAM Modifications}{153}
\contentsline {subsection}{\numberline {8.2.2}Practical Efficiency}{154}
\contentsline {chapter}{\numberline {9}Discussion, Conclusion and Future Work}{157}
\contentsline {section}{\numberline {9.1}Discussion}{157}
\contentsline {section}{\numberline {9.2}Conclusion}{162}
\contentsline {section}{\numberline {9.3}Future work}{163}
\contentsline {chapter}{Primary Bibliography}{164}
\contentsline {chapter}{Appendices}{171}
\contentsline {chapter}{\numberline {A}Protocols}{173}
\contentsline {section}{\numberline {A.1}Path ORAM}{173}
\contentsline {subsection}{\numberline {A.1.1}Access Protocol}{173}
\contentsline {section}{\numberline {A.2}Secret Sharing Path ORAM}{173}
\contentsline {subsection}{\numberline {A.2.1}Building Blocks}{174}
\contentsline {subsection}{\numberline {A.2.2}Trivial Oblivious Access}{175}
\contentsline {subsection}{\numberline {A.2.3}Position Map Oblivious Access}{175}
\contentsline {subsection}{\numberline {A.2.4}Path ORAM}{176}
\contentsline {subsection}{\numberline {A.2.5}Eviction}{177}
\contentsline {section}{\numberline {A.3}Multiplicative Resharing}{179}
\contentsline {section}{\numberline {A.4}SCORAM and Circuit ORAM}{180}
\contentsline {subsection}{\numberline {A.4.1}SCORAM Eviction}{180}
\contentsline {subsection}{\numberline {A.4.2}Circuit ORAM Eviction}{180}
