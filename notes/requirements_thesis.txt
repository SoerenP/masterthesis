-> Under specialet arbejder den studerende selvstændigt med en
faglig problemstilling og kandidaten kan ved specialets afslutning
->på videnskabeligt grundlag identificere, afgrænse og 
formulere en faglig problemstilling

	We need faster MPC and on RAM programs

->definere og opstille testbare hypoteser inden for
fagets emneområde

IVAN: Der bør stilles mål op for det man gerne vil.
Jeg vil gerne finde ud af om påstand x er sand, kan man
så svare på det. Så det kommer mest an thesis statement.
Det passer mest på et eksperimentielt speciale.
Stil mål op og se om man kan nå det. VI SKAL IKKE SVARE PÅ DET!
I.e vi SKAL ikke svare ja eller nej på thesis statement, men vi skal
kunne opstille og motivere det. 

	Kan man bruge RAM model design i MPC?


->selvstændigt planlægge og under anvendelse af fagets
videnskabelige metode gennemføre et større fagligt projekt

	Ja, ligger lidt i det vi har foretaget os

->analysere, kritisk diskutere og persepktivere en
faglig problemstilling

IVAN: Diskussionsafsnittet. Fremtidigt arbejde (perspektivering),
havde jeg haft mere tid hvad ville jeg så have kigget på.
Problemformulering og iscenesættelse(introduktion) er også perspektivering.

	Det er nok meget teori vs praksis ift schemes.
	Perspektivering ved jeg ikke helt.

->vurdere, kritisk analysere og sammenfatte den videnskabelige
litteratur inden for et afgrænset emneområde

IVAN: Man skal ikke tage alt for gospel. Altså ikke blot antage at ting er
sandt fordi det står i litteraturen

	Vi har overordnet set et godt survey, så ja.

	ydermere påpeger vi ting der måske ikke er helt
	rigtigt i literatur ift 2server og folks rakken ned på det
	og vi udforsker påstanden om at prfs er dyre 

	og også litteratur ift lowerbound. 

->formide videnskabelige resultater objektivt og koncist til 
et videnskabeligt forum

IVAN: Forklaring kan sagtens give point. Men du skal kun præsentere
ting der giver mening at vise, du skal kunne argumentere
for at du inkluderer noget, så det skal være relevant for
din problemformulering, fx ved at tillade redskaber og notationer.

ift appendix, hvis du i prosa kan forklare hvad en klods gør,
kan du smide den i appendix så folk kan kigge inde i boxen
hvis de lyster. Hvis du aktivt bruger klodssens indhold
kan den være i selve prosaen. 

	Vi har som sagt et ok survey, så ja. Ved ikke
	hvor objektive vi er.. synes ikke vi vælger
	side på noget tidspunkt.


Så, det vi mangler er at opstille testbare hypoteser
hvilket nok skal komme ud af nogen af
de sidste 4 store ting vi gerne vil kigge på.
Vi skal jo ikke selv afprøve dem, men
det skal være klart at dette er noget man an hypotisere
sig frem til (i.e er flere servere nødvendigvis bedre?
passer server comp. tradeoff godt ind i MPC? yada yada). 
