Three Party ORAM for Secure Computation

An ORAM for secure computation is a protocol which securely implements a RAM funcitonality, i.e given secret sharing of both
data array D, and index N, it computes a secret sharing of D[N]. This can enable secure computation/subprotocols for
implementing ram functionality for secure computation of RAM programs (as opposed to circuits).

Many functionalities are easier in the 3-party case of secure computation. Thus they assume 3 players/Servers and an honest majority.

Previous schemes use Yaos garbled circuits, homomorphic encryption, or SPDZ protocol for arithmetic circuits, their
techniques are custom made for the threeparty setting, giving rise to a protocol which is secure
against a semi honest advrsary, usng bandwidth and CPU cost comparable to the underlying client-server oram (binary ORAM)

###Multiple parties?

You can relax the server-client setting to still have a single client but multiple n > 1 servers. I.e LO did this for 2 servers,
and you can then say that (t,n) threshold, i.e LO is (1,2) (i.e 1 server can be corrupt). 

Stronger notion: n servers emulate multiple clients, but their access patterns remain hidden even if up to t of these
n servers collude, with possible coalition with clients. 

That is, such multi party ORAM simulator is equivalent to the secure computation of the RAM functionality which given
the secret sharing of memory array D and a secret sharing of a location i and value v outputs
a secret sharing of record D[i] (and writes v at index i in the secret shared array).

Motivation: we want oblivious access to RAM in secure computation, as to emulate RAM access as a subprotocol. 




Standard approach to secure computation (S_1,...,S_n compute function f st. the protocol reveals nothing else but the final
outputvalue y = f(x_1,...,x_n) to participants) is to represent f as a boolean circuirt, an arithmetic curcuit.

But even very simple functions, these representations can be impractically large, i.e when some of the data/input to the
circuit is large, we need a gate for each bit of the input (or arithmetic value - nonetheless linear in the size of the input). 

Consider a database of size x_1, a query of x_2, and a function implementing the query called f. f must be at least the size of
x_1, using the above techniques. Yikes!


####Secure Computation ORAM Applications

Can be used to securely compute any RAM program, because each local computation step can be implemented using Yao's garbled circuit
technique, while each memory access can be handled by the SC-ORAM subprotocol, that is, the oram is used as the memory access
abstraction. 

Fx Keller, Scholl recently made datastructures that were oblivious with such a technique, i.e they computed dijkstra,
where the access to edges and vertices etc. was implemented with oblivious datastructures in the above manner (i.e as subprotocols to
the overall computation).

In general good for an information-retrival algorithm since these are developed in the RAM model (identify database
by using hash tables of keywords fx). 

Can also be alternative to PIR, or SSE, if multiple clients can be supported.

####From client server oram to two-party SC-ORAM

one of the two parties can implement the server in the underlying client-server ORAM scheme, while
all the work of the client can be jointly computed by the two parties by using Yao's garbled circuits applied to the circuit repre
sentation of each step of the client algorithm in any client-server ORAM scheme. 

Many other ideas,i.e homomorphic encryption instead of garbled circuits, not clear if the ORAM emulation is faster in this way.

SCORAM/CIRCUIT ORAM recognized the importance of the size of the client, and modified the eviction of PATH oram to make
the client about 10x smaller, and thus, faster, in the above setting.

Results in between 4.6M and 13Mio gates, and a secure evalution required online as many hash or block cipher operations
as the number of non-xor gates in the circuits. 


#####THEIR CONTRIBUTION

Secure computation of many functionalities can be implemented with easier tools in the multi-party setting with 
HONEST MAJORITY than in the two party setting. i.e the classical result in the case of honest majority, the only
assumption we need is secure channels between all parties, then any function can be evaluated securely.

Thus, is it easier to go from 2PC to MPC, as well as search for the smallest possible client circuit?


Keller & Scholl did this with their datastructures, i.e using SPDZ their secret shared both server and client - this
gave significantly faster ONLINE times as compared to SCORAM, i.e 250 ms pr access for 2^20 = N, for 2 machines
compared to 30 seconds of cpu time for SCORAM for N = 2^24. And SPDZ Is actively secure, Yao's garbled (SCORAM) is only honest.

But, the SPDZ approach requires an offline phase between 100 and 800 minutes for m = 20. 

In this work, they focus specifically on the 3-party setting with a single corrupted party. It uses a variant
of binary-tree ORAM as an underlying datastructure. 

Based on the following observation:

	If P_1 and P_2 secret share an array of (keyword, value) pairs (k,v) (a path in binary tree ORAM) and a searched-for
	keyword k* (address prefix) then a variant of Conditional Disclosue of Secret (Secret-Shared Conditional OT) SSCOT
	allows P_3 to recieve value v associated with keyword k* at cost rougly equal to the symmetric encryption and
	transmission of the array (path). 

	Moreove, while SSCOT reveals the location of the pair (K* ,v) in the array to P_3, this leakage
	can be easily masked if P_1,P_2 first shift the secret shared array by a random offset

	The EVICTION part of the SC-ORAM springs from an observation that instead of performing the eviction computation
	on all the data in the path retrieved at access, one can use garbled circuit to enode only the procedure
	of determining eviction movement logic, i.e which entries should be shifted down the path. 

	Then if P1_P2 secret share the retrieved path, and hence the bits which enter this computation
	we can let P_3 evaluate this garbled circuit and learn the positions of the entries to be oved if
		1) the eviction moves a constant number of entries in each bucket in a predictable way (fx one step down)
		2) P1,P2 randomly permute the entries in each bucket so that P_3 always computes a fixed number of
		randomly distributed distinct indexes for each bucket

	Computation of this movement logic uses only two input pits (appropriate direction (left/right), and full/empty flag)
	and 17 non-xor gates per bucket entry, so the garbled circuit is much smaller than if it coded the WHOLE eviction
	procedure.

	Finally, the secret-shared data held by P1,P2 can be moved according to the movement matrix held by P3 in another
	OT/CDS variant we call secret-shared Shuffle OT, which uses only XORs and whose bandwidth is roughly four times
	the size of the secret-shared path.

	Assuming constant record sizes, bandwidth of the resulting protocol is O(w(log^3 N + security par * log^2 N))
	where w is the bucket size of the underlying binary tree ORAM.

	The overflow probability bound requires security parameter = Omega(lambda + log N) where lambda is a statistical
	security parameter, and since log N < security parameter, this assymp. bound is essentially the same as that of
	2-ser SC-ORAM.

	But in practice, it is much lower than SCORAM since:

	The circuits are smaller, even though they also use garbled circuits. 

	All computation outside the garbled circuit is a small factor away from the cost of transmission
	and decryption of server data in the underlying binary tree ORAM.

	I.e non garbled is 9P, where P is the pathsize for the bandwidth
	and 20P bits encryption using symmetric key crypto bounds the computation, whereas the underlying binary tree uses 2P for each.

	Practical parameters can reduce the above by a factor between 3 and 4, if log n = 36 and lambda = 40.


SCORAM had 4.6 M gates for log N = 20, 3PORAM has 96.9K only! for stat security of 80 and bucket size 128. 
CIRCUIT ORAM obtains 350k and gates for Log N = 20. 

If w = 32 only, 3PORAM has only 28.5K gates.

In practice, the bandwidth was a factor of 40 less, took 320 ms per access in online, and 1.3 seconds in pre-computation.
i.e about a factor 50 less. 

3-fold contribution:

	1) immideate improvement over any application of SC-ORAM which ca be done in the setting of three parties with an honest majority.
	2) The techniques can be utilized for a SC friendly eviction strategy for Binary Tree ORAM
	3) the proposed protocol has several avenues for possible optimization, both on system level and in the algorithm design.

###TECHNICAL OVERVIEW

combination of garbled ciruict, 3-party OT, and hierarchial two party ORAM of Shi et al.

The observation is that in eviction algoritms for Tre ORAMs as well as access, there is a seperation in the role of the
input bis of the access and eviction circuit.

Part of the bits implement the logic of the circuit, but the rest do not contribute to the logic, but are at best, just
moved to different positions. 

This is exploited in three party setting, by using Yaos garbled circuit to compute only on the bits, and then use OT variants
to move the data based on the output of the logic, i.e the positions pointed to. OT cost is similar to secure transmission of the data.

Further, the access protocol does not use garbled circuits at all, since access can be seen as
finding an index where two lists of n bitstrings contain a matching entry, which is implemented by
using 3-party condiition OT variant, in a single round, costs roughly the same as encryption + transmission of the n bitstrings.

They make several modifications to the binary tree oram to make it fit better to their intentions. 

Fx they make the tree more shallow by increasing how many entries in the ORAM will be mapped to each leaf in expectation
and increasing the capacity of leaf nodes.

for a tree of 2^m entries, and node capacity w, we instead make 2^m/w leaves as opposed to 2^m. 

To ensure no overflow, leaves are increased to 4w capacity. 

Can thus store O(2^m) entries instead of O(w * 2^m). (for most settings w = log N).

But we do not increase capacity of internal nodes, since these are governed by different stochastic processes,
and the bandwidth does not explode, but only increases by a constant. 

EVICTION is done on a single path. The problem with the previous idea is that all entries are evicted as deep as possible along
the path. This is expensive when we are in the setting of MPC/SC since the computations will be on secret data, as opposed
to the ram model, where this is done on plaintext values (easy).

Thus, modify the eviction to at most evict 2 items pr bucket. This gives reasonable circuits. 
Its oblivious, and gives a simple 3 party protocol. 

NO STASH OR FANOUT USED as this would increase and complicate the circuits. 


###COMPARED TO 2-SERVER

we rely on non-colusion in both 2-server and 3-server. The 2-server achieves log n amortized overhead, the asymptotic
running time of this two-server ORAM could also be linear in log N, which would beat (asymp) any thing based on the O(log^3 N)
sngle server ROAM schemes.

But the 2-server ORAM has some features that adversely affects practicality of the resulting SC-ORAM protocol.

It is a hierarchial construction, with O(log N) levels, where the i-th level contains O(2^i-1) encrypted memory entries
After every 2^t RAM access, the construction reshuffles the first 2^t levels of hierarchy incurring O(2^t) cost,
which makes access highly uneven.

The retrival algorithm has to be emulated with secure computation, acts differently on a given level depending
on whether the item has been found on a higher level (branching). 

Seems to also require PRF with secret-shared inputs, done obliviously, (possibly also outputs), and such constructions
require O(t) exponentiations for a t-bit domain.


####BASELINE CLIENT SERVER-ORAM PROTOCOL

Take a tree based binary ORAM scheme but make it secure computation friendly - that is, few computations on secure
/ secret values (small client and eviction) but without making the parameters, and thus the ORAM, perform worse (i.e
too much computation/bandwidth).

ORAM FOREST:
	
Let D be an array of |D| <= 2^m records, where D[N] for every m-bit address N is a bistring of fixed length d.

The oram protocol only needs O(k = crypto sec. parameter) persistent storage, and O(m * d) transient storage (a path),
and O(2^m * d) = O(|D|) persistent server storage. 

Let h = log_2^t 2^m = m/t, where t is an iteger divisor of m.

Server S stores an ORAM FOREST OTF = (OT_0, OT_1,...,OT_h). Each ORAM TREE OT_i for i > 0 is a binary tree
of height d_i = it - log w (where w = max(lambda,m), i.e bucketsize).

Let N = [N^(1)],...,[N^(H)] be the parsing of N into t-bit segments and let N^i = [N^(1)]...[N^(i)] be N's
prefix of length ti (i.e we split an address up into bits, where each bit segment is used to address into one of the h ORAMs
in the forest of ORAMs!).

The last ORAM tree OT_h implements a look up table F_h, st F_h(N) = D[N]. BUT! The efficient retrieval
of F_h(N) from OT_h is possible only given a label L^h \in {0,1}^d_h, which defines a leaf (i.e path)
in OT_h, where value F_h(N) is stored. The way this label L^h can be found is that each ORAM
tree OT_i for i < h implements a look up table F_i which maps N^i to label L^i+1 \in {0,1}^d_i+1
and it is an ivnariant of OTF that for each i, L^i+1 = F_i(N^i), i.e defines a leaf/path in OT_i+1
which contains a value of F_i+1 on arguments N^i+1 = N^i|N^(i+1) for all N^(i+1) \in {0,1}^t.

This just means that we look up the address we need in the next ORAM recursively, until we've looked in them all.

That is, the ACCESS algorithm retrieves L^1 = F_0(N^1) from OT_0, then using L^i, retrieves L^i+1 = F_i(N^i+1)
from OT_i for i = 1....h-1, and finally using L^h it retrieves F_h(N) = D[N] from OT_h. 

Another way to look at is that the forest implements the function F_OTF: {0,1}^m -> {0,1}^d_1 x ... x {0,1}^d_h x {0,1}^d
where F_OTF(N) = (F_O(N^1),F_1(N^2),...,F_h-1(N^h),F_h(N^h))


How are F_i values stored in binary tree OT_i?

Let {0,1}^<m denote the binary strings of length 0 to m-1.

The nodes of OT_i for i > 0 are formed:

	-> Each internal node indexed by j \in {0,1}^<d_i stores a bucket B_j, while each leaf node indexed by
	j \in {0,1}^d_i is a set of four buckets (B_j00,B_j01,B_j10,B_j11).

	->Tree OT_0 is an exception because it consists of a single (root) node B_root. (the constant of 4 is somewhat arbitrary)

	-> Each bucket B_j is stored at node j in OT_i encrypted under a master key held by C.

	-> each bucket is an array of w tuples of the form

		T^i = (fb_i, N^i, L^i, A^i) where

			-> fb_i \in {0,1} (empty flag)
			-> N^i \in {0,1}^it (address)
			-> L^i \in {0,1}^d_i (leaf)
			-> A^i for i < h is an array containing 2^t labels L^i+1 \in {0,1}^d_i+1
			while A^h is a record in D. 

The above invariant is maintained if for every N (or if D is sparse, only for those N's for which D[N] is non-empty)
there is maintained a sequence of labels (L^1,...,L^h) (assume L^0 = 0 (root) and N^(0) = 0^t, i.e only one address)
st. each OT_i contains a unique tuple of the form 

	T = (1, N^(i), L^i, A^i)

for some A^i and

	1) this tuple is contained in some bucket along the path from the root to the leaf L^i in OT_i
	2) if i < h, then A^i[N^(i+1)] = L^i+1 and A^h = D[N]

Observe that (L1,...,L^h, D[N]) = F_OTF[N]


#####ACCESS PROCEDURE

To access location N in D, the client C performs the following loop sequenctially for each i = 0,..,h, given
the recursively obtain L^i:

1) C sends L^i to the server S, who retrieves and sends to C the encrypted path P_L^i in tree OT_i from the root to leaf L^i
2) C decrypts P_L^i using its master key into a correspinding plaintext path P_L^i, which is a sequence of buckets
(B_1, ..., B_n) for n = d_i + 4 (leafes have 4 buckets)
3) C finds the unique tuple T^i in this bucket sequence of the form T^i = (1, N^(i), L^i, A^i) and computes either
label L^i+1 = A^i[N^(i+1)] if i < h OR
outputs A^i as the record D[N].

Note that the protocol reveals the vector of labels (L^1,...,L^h) to Server S.
Therefor each access C picks new random lables ((L^1)',...,(L^h)') where (L^i)' is a
random bitstring of length d_i, and OTF needs to be updated so that F_OTF(N) = ((L^1)',...,(L^h)', D[N]).

To do this, C erases the tupple T^i = (1, N^(i), L^i, A^i) in the bucket in which it was found (by flipping the fb field to 0)
replaces L_i with (L^i)', sets A^i[N^(i+1)] to (L^i+1)', and inserts this modified tuple (T^i)' into
the root bucket B_1. 

C then reencrypts the buckets and sends the new encrypted Path P_Li' to S to insert in place of P_Li in OT_i


###COSTRAINED EVICTION STRATEGY

The above works, except that since we write the modified tupple to the root, the root fills up after w acceses.

To ensure that this does not happen (with overwhelming probability) an eiction step is interjected into the access protocol
before the client reencrypts the path and sends it back to S.

The goal is to move each tupe T = (N^i, L, A) in an internal node of tree OT_i down towards
its destionation leaf L.

Since in access C reads only the tuples in path P_L_i, this will only be done for tupples found in the internal buckets
of the path.

We want this to be as easy to compute as possible in the 3-party setting, so we restrict this eviction in two ways:

	1) we will attempt to move at most two tuples down in every path
	2) we will move them only one bucket down.

These ONLY MAKE SENSE in the context of SC/MPC, since in the client-server ORAM in the RAM model, it is no harder to move
things further down that the above, since the client sees the plaintext of the path.

The data movement pattern will be more predictable, which means that we get a circuit that DOES NOT implement the
whole entire eviction as a single securely-computed circuit.


Consider a bucket B_-j corresponding to an internal node in P_L^i, i.e for j <= d_i.

We say taht tuple T = (fb, N^i, L, A) in B_j is MOVABLE down the pth towards leaf L^i
if fb = 1 (non-empty) and the j-th bit in its label field L matches the j-th bit of leaf L^i 
(since its a binary tree, the bits denotes which way we branch!)

In every bucket B_j for j <= d_i we choose two random tuples which are moveable down towards leaf L^i.

If there are no such tuples which are moveable, we choose two random emppty tupples instead, if they exist, and if there
is only one then the second is chosen as a random empty tuple (if it exists). In addition, we choose two random empty
tuples (if they exist) among the 42 tuples contained i the four buckets B_j contained in the leaf node in P_L^i,
i.e for j = d_i + 1,...,d_i+4 then for each i <= d_i, we take the two chosen tuples in bucket B_j and move them to
the two spaces vacated in bucket B_j+1 (except for j = d_i, where the two chosen spaces in the level below can be in
any of the buckets B_di+1,...,B_di+4)


Eviction FAILS in case of a bucket overflow i.e if
	1) some internal bucket in P_Li does not contain two tuples which are either EMPTY or MOVEABLE
	2) the four buckets corresponding to the leaf node do not contain two empty tupples.

Both prob of the above are negligible for w = O(m) assuming accesses do D is polynomial in |D|. i.e any efficient 
usage of D will only fail with negligible probability. 


####NOTATION

|P_L^i| = length of any path in tree OT_i 
|T_i| = length of any tuple in such a tree

IF i < h (i.e not the full final ORAM)
	|P_L^i| = (d_i+4)*w*|T^i|
	|T^i| = 1 + i * t + d_i + 2^t * d_i+1
	|T^h| = 1 + h * t + d_ h + d because T^i = (fb, N^i, L^i, A^i), |N^i| = i * t, |L^i| = d_o,
	and A^i for i < h is an array holding 2^t next-level leaf labels of length d_i+1, while
	A^h = D[N] and |D[N]| = d. 



####THREE PARTY PROtOCOL BUILDING BLOKS

Access, PostProcess, Eviction rely on several variants of Oblivious Transfer or Conditional Disclosure of Secrets.

All OT variants, also those in Yao's protocol, have significantly cheaper realization in the 3-party setting! (cheating?)

ALL presented protocols assume secure channels, althought in many instances encryption overhead can be eliminated with
simple protocol changes, (pairwise shared keys in PRG and PRFs)


NOTATION:
let k denote the crypto security parameter, which will assume is both the key length and the block length of a 
symmetric cipher.

Let G^L be a PRG which outputs l-bit strings given a seed of length k.

Let F_k^l be a PRF which maps domain {0,1}^k onto {0,1}^l, for x randomly chosen in {0,1}^k

We write G and F_k when L = k. 

In practice, they used counter mode AES for both.

If A holds a, B holds B s.t a XOR b = v, then we call (a,b) an "A/B secret sharing" of v, and denote it 
(S_A[v],S_B[v]).

INTENTED OUTPUT of some protocol being A/B sharing of a value v, means that its a RANDOM XOR-sharing,
i.e a par (r, r XOR v), r \atrandomin {0,1}^|v|. Let s[j] denote the j-th bit of bitstring s, and let [n] denote the integer range
{1,...,n}.



####3-party variants of oblivious transfer

For all protocols in the following, almost, the precomputation and online cost is within a factor 2 of the cost of secure
transmission of the senders input. They all require nly one ore two messages in both the precomputation and online phase.

All protocols form secure computation protocols of the corresponding functionalities, ASSUMING
AN HONEST BUT CURIOUS ADVERSARY, SECURE CHANNELS and an HONEST MAJORITY / SINGLE CORRUPTED PLAYER

For all the security is straight forward simulation argument.


###SECRET SHARED CONDITIONAL OT, SSCOT(S,R,H)

S inputs two lists (m1,...,mN) and (a1,..,aN), H inputs a single list (b1,...,bN) and 
the protocols goal is for R to output all pairs (t,m_t) s.t a_t = b_t, can be implemented with mod arithmetic
in a prime order field. Details in paper


###Secret-Shared Index OT, SS-IOT(S,R,H)

S holds a list of messages (m0,---,mN-1) for N = 2^t, and an index share j_s \in {0,1}^t
while H holds the other share j_H \in {0,1}^t, and the aim of the protocol is for R to output (j, m_j) st. j = j_s XOR j_H


####Shuffle-OT, XOT choose[N,k](S,R,I), sender S, reciever R, indicator I

S inputs a sequence of messages m1,...,mN, 
I inputs a sequence of indexes i_1, ..., i_k and a sequence of masks z1,...,zk

the protocol lets R output a sequence of messages m_i1 XOR z1,...,m_ik XOR z_k, without leaking anything
else about S's and I's inputs.


###SECRET SHARED SHUFFLE OT, SS-XOT choose[N k] (A,B,I)

Indicator I and two parties, A and B.

I holds indexes i = (i_1, ..., i_k) and pads z1,...,zk are all set to zero,
and both inputs m1,...,mk and outputs m_i1,...,m_ik are secret shared between A and B.

The output is a randomized A/B secret sharing of (mi1,...mik)



####SHifting a secret shared sequence, Shift(A,B,H)

The buckets in the path are rotated by a random shift alpha_i, known to D and E,
so we need to rotate this path back with a fresh secret sharing of the shifted path, shifted back alpha_i places.

Very similar to SS-IOT


####Yaos Garbled Circuits on Secret-Shared inputs

The last component is GC[F](A,B,R) a Yao's garbled circuit solution for secure computation
of an arbitrary function executing on public input a circuit function F, where the inputs X to this circuit
are secret shared between A and B, i.e A inputs S_A[X] and B inputs S_B[X], and the protocol lets 
R compute F(X). 

This is used sparingly as a subprotocol, comparable in cost to the neccesary encryption/decryption of paths, for realistic m values.

The input is secret shared by parties A and B, while the third party R will compute the garbled circuit and get the F(X).

Let N = |X| and let k be the bitlength of the keys used in Yaos garbled circuit.

In the offline stage either A or B, say party A, prepares the garbled circuit for function F and sends it to R,
and then for each input wire key pair (K^0_i,K^1_i) created by Aos circuit garbling procedure,
A picks random delta_i in {0,1}^k, computes (A^0_i,A^1_i) = (delta_i, K^0_i XOR K^1_i XOR delta_i) and
(B^0_i, B^1_i) = (K^0_i XOR delta_i, K^1_i XOR delta_i) and sends these to B

In the online phase for each i = 1....n party A on input bits a = S_A[X] sends A^a_i to R, while party B on input bit b = S_B[X_i]
sends B^b_i. 

For each i = 1 ... n, party R computes K_i = A_i XOR B_i for A_i, B_i recieved respectively
from A and B, and then runs Yao's evaluation procedure inputting keys K_1,..,K_n into the garbled circuit recieved for F.

####THREE PARTY SCORAM PROTOCOL

Three party secure computation of the previously described ORAM.

Denote parties as C,D and E.

Idea: Secret share the datastructure OTF between two servers D and E, and have these two parties implement the servers
algorithm of the client-server ORAM scheme, while the corresponding client algorithm will be implemented
as a three party secure computation involving parties C,D,E. 

These two parts are combined into a single protocol. The server algorithm just reads and writes the path into the
secret shared forest.

The task is to securely compute the following to implement the protocol:

	1) the ACCESS functionality computes the next-tree label L^i+1 = F_i(N^i+1)
	given D/E secret sharing of path P_L^i, for L^i = F_i-1(N^i) and the D/E secret sharing of prefix
	N^i+1
	2) the EVICTION functionality computes the D/E secret sharing of path P^*_Li output by the eviction algorithm applied
	to the D/E secret shared path P_L^i
	after the tuple containing the label identified by the access functionality is moved to the root node.

The i-th iteration requires a server-to-client transmission and decryption of path P_L L^i and then
encryption and client-to-server transmission of path P^* L^i. They thus want to come close
to h+1 rounds of client-server interaction wth 2 * |P^i_L| bandwidth and (2/k) * |P^i_L| block cipher operations 
for i = 0,...,h.

The main idea which allows them to come close to these parameters is that if the inputs to either access or eviction functionalities
secret shared by two parties are shifted/permuted/rotated/masked in an appropriate way, then the correspondingly shifted/masked output
of these functionalities can be revealed to the third party, e.g C.

We separate the client-server access/eviction into
Access, PostProcess, and Eviction.

Access contails all parts of the client-server access which have to be executed sequentially, i.e
the retrieal of the sequence F_OTF(N) = (L^1, L^2, ... , D[N]) done by sequential identification and removal from OT_i
trees of the tupple sequence (T^1, T^2, ..., T^h) where T^i is defined as path P_L L^i of tree OT_i whose address field is equal
to N's prefix N^i, and whose A field contains label L^i+1 at position N^(i+1).

PostProcess perfroms cleaning-up operations on each tuple T^i in this tuple sequence, by modifying its label field
from L^i to (L^i)' and modifying the label held at N^(i+1)'th position in the A^i array of this tuple from L^i+1 to (L^i+1)'.

PostProcess and Eviction can be done in Parallel for all trees OT_i, which allows better CPU optimization.

####ACCESS PROTOCOL

runs D/E secret sharing of searched-for address N, and the ORAM forest OTF, its goal is to compute a D/E secret-sharing of 
record D[N].

Creates two additional outputs for each i = 0, .. ,h (with some parts skipped in the edge cases of i = 0, and i = h)
	1) C/E secret sharing of the path P_L^i in OT_i with T^i removed
	2) whatever information needed for the PostProcess protocol to modify T^i into (T^i)' which will be
	inserted into the root of OT_i in protocol Eviction

	The protocol basically for each entry in the forest looks up the label to use in the next, and performs a rotation
	on the data in the path P_L^i

They apply three rotations: Rotate all blocks in the path by some offset. Rotate all records in each bucket by some offset.
Rotate the A array by doing an index matrix (permutation). This can all be done locally by the parties, since it 
amounts to simply adding something to their local shares of the above.

Then two of the ot protocols are performed, SS-COT and SS_IOT, which allows the parties to get the tupple that contains
the searched for address prefix N^i, as well a secret sharing of these. It also allows them to extracct the next label from
the A array of the now found secret shared tupple, so they can continue to the next recursive lookup in the forest.

The actual details are quite hairy, look in the paper. 

#POSTPROCESS

The goal here is to inject the updated mapping L^i -> (L^i)' into the secret shared array of leaf mappings.

With some preprocessing this can be done with some reveals. Look in paper for details


####EVICTION

Is done in parallel for the entire forest of ORAMS.  The circuit only uses 16 * w non-xor gates. SInce it only uses
the i-th bit of a leaf label and the i-th but of label L^i defining path P*L_i).



####ANALYSIS:

w = bucket size
|D| (array) = 2^m


bandwidth is O(w(m^3 + security * m^2))
		path		garbled circuit


local computation O(w(m^3 / security + m^2)) block cipher or hash operations, the first term is data transmition in
the client-server ORAM.

The GC computation contributes O(wm^2) hash function operations (performed by one party only)

m < k, so O(wm^2) could dominate in local computation.


PR(leaf node overflows) = negligible <= 2^-security IF 2w >= secuirty + log N + m + 2log m


Pr(internal bucket overflow) = negligible <= 2^-security IF w - log w >= security + log N + 2 log M + 1

for w < 512 this can simplified to w >= security + log N + 2 log m + 10.

The bounds above are "pessimistic" since they used union bound (which overshoots quite a bit). 

In practice they used some markov chain analysis to get better bounds for their testing.


#####TESTING

they build and benchmarked a prototype java implementation of the proposed 3-party SC-ORAM protocol. 

bit length of RAM address m was between 12 and 36, and record size d ranget from 4 to 128 bytes. 
They set w (bucket size) to 16, 32, 65 and 128, and for each combination of the 3 above parameters, they
made grid search for the segments that divide the RAM address, i.e t.

In all cases, the optimal t seemed to lie between 3 and 6. 

Running time had linear relationship with bucketsize. 

The fraction of time spent on the garbled circuits decreased from 50% to 25% going from m = 12 to m = 36.

Half the time of garbled circuits was spent on SHA evaluation (i.e decryption of gates). 

The computation for symmetric ciphers lay between 10% to 5%. 

The handling of message passing to and from TCP communication sockets grew from 12% to 30% for the same range of M.

Concrete times: m = 35, 400 ms to look up single element
		m = 20, 100 ms to look up single element


### conclusion (they didnt have one)

They focus only on the case of 3-parties (since you can do nifty tricks with OTs) and semi honest security.

Their idea is similar to other recent research - if we want to do oram in secure computation, we need to reduce
the size of the client. They only use GC on the strictly neccesary parts of the client to make
the overhead really small, in particular, the input to the eviction algorithm is just a few bits in total,
since you only need a string that describes a leaf path (a bitstring, 0 go left, 1 go right)
and a leaf mapping of a given record, so see if you can evict down a given path (i.e do the two bitstrings agree
on the corresponding bith,if yes, then you can move it) so the circuit to evict does not need to be able to handle
an entire path, but only a few bits.

They take advantage of the 3-player setting by assuming that only 1 party is corrupted, by doing a lot of OT
tricks where they argue all the time that they can simply reveal stuff to each other, since two players cannot
help each other if they are evil.

They are about 10-20x times, or even more, faster than SCORAM in practice, with even smaller circuits.

BUT! only the 3-party case, and honest majority. So, it is impressive, but it is really hardcoded to that
specific case, and does not naturally scale to multiple parties.
