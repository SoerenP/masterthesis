The reason line 8 in evictonceslow is not oblivious is that
it skips a level, and we would want to do
fake evictions at each level, when we skip, we leak that
we just eviccted successfully, and an observer can then track entries.

the eviction algorithms can be made to appear oblivious since
we can use IfElse and conditional write flags for each single level,
and as such we then touch each entry on each level of the evicted path
which is then oblivious since the path is chosen at random.
