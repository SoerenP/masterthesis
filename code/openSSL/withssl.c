#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <stdio.h>
#include <string.h>

/* to compile, link with: gcc foo.c -o foo -lssl -lcrypto
 * or in makefile
 * LDLIBS = -lssl -lcrypto
 *
*/

#define CERT_PATH "/usr/lib/ssl/certs"
#define BUFF_SIZE 1024

int main()
{
    BIO * bio;
    SSL * ssl;
    SSL_CTX * ctx;

    int p;

    char * request = "GET / HTTP/1.1\x0D\x0AHost: www.verisign.com\x0D\x0A\x43onnection: Close\x0D\x0A\x0D\x0A";
    char r[BUFF_SIZE];

    /* Set up the library */

    ERR_load_BIO_strings();
    //SSL_load_error_strings();
    SSL_library_init();
    OpenSSL_add_all_algorithms();

    /* Set up the SSL context */

    ctx = SSL_CTX_new(SSLv23_client_method());

    /* Load the trust store */

    /* just specify the folder and let OpenSSL search for a valid one */
    if(! SSL_CTX_load_verify_locations(ctx, NULL, CERT_PATH))
    {
        fprintf(stderr, "Error loading trust store\n");
        ERR_print_errors_fp(stderr);
        SSL_CTX_free(ctx);
        return 0;
    }

    /* Setup the connection */

    bio = BIO_new_ssl_connect(ctx);

    /* Set the SSL_MODE_AUTO_RETRY flag */

    BIO_get_ssl(bio, & ssl);
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY); //if new handshake requested by server, automatically do it

    /* Create and setup the connection */

    BIO_set_conn_hostname(bio, "www.verisign.com:https"); //do connection

    if(BIO_do_connect(bio) <= 0) //handshake after aboe connection
    {
        fprintf(stderr, "Error attempting to connect\n");
        ERR_print_errors_fp(stderr);
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return 0;
    }

    /* Check the certificate */

    if(SSL_get_verify_result(ssl) != X509_V_OK) //is the certificate up to date?
    {
        fprintf(stderr, "Certificate verification error: %li\n", SSL_get_verify_result(ssl));
        BIO_free_all(bio);
        SSL_CTX_free(ctx);
        return 0;
    }

    /* Send the request */

    BIO_write(bio, request, strlen(request));

    /* Read in the response */

    for(;;)
    {
        p = BIO_read(bio, r, BUFF_SIZE-1); //always request almost all we can contain
        if(p <= 0) break; //read until you cant
        r[p] = 0; //null terminate the string
        printf("%s", r); //print the string so far. 
    }

    /* Close the connection and free the context */

    BIO_free_all(bio);
    SSL_CTX_free(ctx);
    return 0;
}
