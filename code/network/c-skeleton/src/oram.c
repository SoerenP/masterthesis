#include <oram.h>
#include <stdio.h>
#include <string.h>

struct block block_create()
{
	struct block new;
	memset(&(new.data),'\0',BLOCKSIZE);
	return new;
}

void block_write(struct block *b, char *data, size_t length)
{
	memcpy(b->data,data,length);
}
