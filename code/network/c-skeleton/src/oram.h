#ifndef _ORAM_H_
#define _ORAM_H_

#include <stddef.h>

#define BLOCKSIZE 1025
#define OPSIZE 1
#define READ 'r'
#define WRITE 'w'

struct block {
	char data[BLOCKSIZE];
};

struct block block_create();

void block_write(struct block *b, char *data, size_t length);

#endif
