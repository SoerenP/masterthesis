#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <oram.h>
#include <../src/dbg.h>

int main(void)
{
	int sockfd = 0;

	char recvBuff[BLOCKSIZE];
	char sendBuff[BLOCKSIZE];
	struct sockaddr_in serv_addr;

	memset(recvBuff, '\0', BLOCKSIZE);
	memset(sendBuff, '\0', BLOCKSIZE);

	/* socket returns < 0 on fail, get a socket otherwise */
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	check(sockfd > 0, "Error: Could not create socket");

	serv_addr.sin_family = AF_INET; //local process
	serv_addr.sin_port = htons(5000); //port to listen to
	serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); //local host, since local process

	/* try to connect to the above */
	int rc = connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	check(rc == 0, "Failed to connect");

	printf("Connected\n");

	/* as long as we can read stuff from the server */
	int recv_size = 0;
	while(1)
	{
		printf("Enter message: ");
		char *rc = fgets(sendBuff, BLOCKSIZE, stdin);
		check(rc != NULL, "Fgets error\n");

		int send_size = send(sockfd, sendBuff, BLOCKSIZE, 0);
		check(send_size > 0, "Send Failed");

		int recv_size = recv(sockfd, recvBuff, BLOCKSIZE, 0);
		check(recv_size > 0, "Recieved Failed: %d\n",recv_size);

		printf("Server Reply: %s\n", recvBuff);

		memset(sendBuff, '\0', BLOCKSIZE);
		memset(recvBuff, '\0', BLOCKSIZE);

	}

	close(sockfd);

	return 0;
error:
	close(sockfd);
	return 1;

}
