#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <oram.h>

#define ERROR (-1)

void request_handler(int sock, struct block *bucket, char *buffer)
{
	char operation = buffer[0];
	if(operation == READ)
	{
		int wc = write(sock, bucket->data,BLOCKSIZE);
	} 
	else if (operation == WRITE) 
	{
		memcpy(bucket->data,buffer+OPSIZE,BLOCKSIZE-OPSIZE);
		int wc = write(sock, bucket->data, BLOCKSIZE);
	}
}

void error(const char *msg)
{
	perror(msg);
	exit(1);
}

int main(void)
{
	/* file descriptors for listener, and connection */
	int listenfd = 0;
	int connfd = 0;

	struct sockaddr_in serv_addr;
	
	char recvBuff[BLOCKSIZE];

	struct block bucket = block_create();

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	
	if(listenfd != ERROR){
		printf("Socket Retrieve Success\n");
	} else {
		printf("Failed to retrieve socket\n");
		return ERROR;
	}

	memset(&recvBuff, '\0', BLOCKSIZE);

	serv_addr.sin_family = AF_INET; //local process
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); //convert to network byte order
	serv_addr.sin_port = htons(5000); //host byte order

	/* bind socket to port */
	bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

	/* start accepting connections */
	if(listen(listenfd, 10) == ERROR){
		printf("Failed to listen\n");
		return ERROR;
	}

	int read_size = 0;

	/* since we dont specify O_NONBLOCK, the below accept will block until a single client connects i.e socket_client */
	connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
	if(connfd < 0) error("Error on accept");
	while(1){
		if((read_size = recv(connfd, recvBuff, BLOCKSIZE,0) > 0)){ 
			printf("read: %s\n", recvBuff);
			request_handler(connfd, &bucket, recvBuff);
			memset(recvBuff, '\0', BLOCKSIZE);
		} else {
			printf("read failed: %d, errno: %d\n",read_size,errno);
			break;
		}
		
		sleep(1);
	}
	close(connfd);
	close(listenfd);
	return 0;
}
